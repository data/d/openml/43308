# OpenML dataset: Fish-market

https://www.openml.org/d/43308

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
This dataset is a record of 7 common different fish species in fish market sales. With this dataset, a predictive model can be performed using machine friendly data and estimate the weight of fish can be predicted.

Acknowledgements
Thanks to all who make Kernels using this dataset and also people viewed or download this data.

Inspiration
Multiple linear regression is a fundamental practice for this dataset. Multivariate analysis can also be  performed.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43308) of an [OpenML dataset](https://www.openml.org/d/43308). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43308/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43308/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43308/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

